﻿#pragma once
#include <unordered_map>

namespace Trity {
	
	enum Trit {
		Unknown,
		False,
		True
	};

	typedef unsigned int uint;
	Trit operator &(const Trit left, const Trit right);
	Trit operator |(const Trit left, const Trit right);
	Trit operator !(const Trit value);
	std::ostream& operator <<(std::ostream& out, const Trit value);
	class TritSet {
	private:
		class TritHelp {
		private:
			TritSet &set;
			uint index;
		public:
			TritHelp(TritSet &set, uint n) : set(set), index(n) {}
			~TritHelp(){}
			TritHelp &operator=(Trit value); //перегрузка оператора присваивания
			operator Trit() { return set.znan(index); }
			TritHelp &operator=(const TritHelp &value);
			friend std::ostream& operator <<(std::ostream& out, const TritHelp &value);//вывод трита из класса в поток
			friend bool operator ==(const TritHelp &set, Trit Value);//операции сравнения тритов
			friend bool operator ==(const TritHelp &set_one, const TritHelp &set_two);
			friend bool operator ==(Trit Value, const TritHelp &set);
			friend bool operator !=(const TritHelp &set, Trit Value);
			friend bool operator !=(const TritHelp &set_one, const TritHelp &set_two);
			friend bool operator !=(Trit Value, const TritHelp &set);
			//friend TritSet operator &(const TritSet &left, const TritSet &right);// логические операции между классами
			//friend TritSet operator |(const TritSet &left, const TritSet &right);
		};
		uint size_mass = 0;//размер массива с тритами
		uint *mass = nullptr;//массив тритов
	public:
		TritSet(uint size) {
			if (size != 0) {
				size_mass = size / (4 * sizeof(uint));//сократил формулу size*2/8/sizeof(uint)
				if (size % (4 * sizeof(uint)) != 0) {
					++size_mass;
				}
				mass = (uint*)calloc(size_mass, sizeof(uint));
			}
		}//конструктор

		TritSet() {
			mass = nullptr;
			size_mass = 0;
		}//конструктор

		TritSet(TritSet &value) {
			size_mass = value.size_mass;
			mass = (uint*)calloc(size_mass, sizeof(uint));
			memcpy(mass, value.mass, size_mass);
		}//конструктор копирования

		TritSet(TritSet &&value) {
			size_mass = value.size_mass;
			mass = value.mass;
			value.mass = nullptr;
			value.size_mass = 0;
		}//конструктор move 

		~TritSet() {
			if (mass != nullptr) {
				free(mass);
				mass = nullptr;
			}
		}//деструктор

		Trit znan(uint numb);//вывести значение трита
		void change(Trit value, uint numb);//записать значение трита
		void shrink();// освобождение памяти до начального значения или
					  //до значения необходимого для хранения последнего установленного трита
		uint capacity(); //количество выделенной памяти
		uint length(); //индекс последнего не Unknown трита + 1
		uint cardinality(Trit value);//число установленных в данное значение тритов
									 //для трита Unknown - число значений Unknown до последнего установленного трита
		std::unordered_map<Trit, uint, std::hash<uint>> cardinality();//аналогично но сразу для всех типов тритов
		void trim(uint LastIndex);// забыть содержимое от lastIndex и дальше

		TritHelp operator[](uint n); //помечаем трит с которым будем работать
		TritSet &operator !();//инверсия 
		TritSet& operator=(TritSet&& tmp);//move присваивание
		TritSet& operator=(const TritSet& value);//обычное присваивание
		TritSet &operator&=(TritSet &value);//префиксная коньюнкция
		TritSet &operator|=(TritSet &value);//префиксная дизъюнкция
		
		friend TritSet operator &(TritSet &left, TritSet &right);
		friend TritSet operator |(TritSet &left, TritSet &right);
		friend std::ostream& operator <<(std::ostream& out, const TritHelp &value);
		friend bool operator ==(const TritHelp &set, Trit Value);
		friend bool operator ==(const TritHelp &set_one, const TritHelp &set_two);
		friend bool operator ==(Trit Value, const TritHelp &set);
		friend bool operator !=(const TritHelp &set, Trit Value);
		friend bool operator !=(const TritHelp &set_one, const TritHelp &set_two);
		friend bool operator !=(Trit Value, const TritHelp &set);
	};

}

