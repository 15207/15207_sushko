﻿#include <iostream>
#include <cstdlib>
#include <cstring>
#include "tritset.h"
#include "gtest\gtest.h"

namespace Trity {

	Trit operator &(const Trit left, const Trit right)
	{
		if (left == False || right == False)
		{
			return False;
		}
		if (left == True && right == True)
		{
			return True;
		}
		return Unknown;
	}


	Trit operator |(const Trit left, const Trit right)
	{
		if (left == True || right == True)
		{
			return True;
		}
		if (left == False && right == False)
		{
			return False;
		}
		return Unknown;
	}

	Trit operator !(const Trit value)
	{
		if (value == True) {
			return False;
		}
		if (value == False) {
			return True;
		}
		return Unknown;
	}

	std::ostream& operator <<(std::ostream& out, const Trit value)
	{
		if (value == True)
		{
			out << "True";
			return out;
		}
		if (value == False)
		{
			out << "False";
			return out;
		}
		out << "Unknown";
		return out;
	}

	Trit TritSet::znan(uint NeedTrit) {//вывести значение в данный момент помеченного трита
		uint NeedUint = NeedTrit / 4 / sizeof(uint);
		if (NeedUint >= size_mass) {
			return Unknown;
		}
		uint rezerv = mass[NeedUint];
		uint NeedTritInBit = (NeedTrit % (4 * sizeof(uint))) * 2;
		rezerv = rezerv >> NeedTritInBit;
		rezerv = rezerv & 3;
		return (Trit)rezerv;
	}
	void TritSet::change(Trit value, uint NeedTrit) {//записать значение
		uint NeedUint = NeedTrit / 4 / sizeof(uint);
		if (NeedUint >= size_mass && value == Unknown) {
			return;
		}
		if (NeedUint >= size_mass) {
			uint *mass_support = (uint*)calloc(NeedUint + 1, sizeof(uint));
			memcpy(mass_support, mass, size_mass);
			free(mass);
			mass = mass_support;
			size_mass = NeedUint;
		}
		uint help = ~0;
		uint help_two = ~0;
		uint NeedTritInBit = (NeedTrit % (4 * sizeof(uint))) * 2; // ((8 * sizeof(uint)) / 2)     //вычислить нахождение трита по модулю данного типа
		unsigned char sem = NeedTritInBit + 2;
		if (sem == 32) {
			help = 0;
		}
		else {
			help = help >> sem;//NeedTritInBit  // (NeedTritInBit - 1) + 2; //формула арифметической прогрессии для данного сдвига
			help = help << sem;
		}
		sem = (8 * sizeof(uint) - NeedTritInBit);
		if (sem == 32) {
			help_two = 0;
		}
		else
		{
			help_two = help_two << sem; //((8 * sizeof(uint))-2) - (((NeedTritInBit - 1) * 2) + 2)
			help_two = help_two >> sem;
		}
		help = help | help_two;
		uint *rezerv = &mass[NeedUint];
		*rezerv = *rezerv & help;
		help = value;
		help = help << NeedTritInBit;
		*rezerv = *rezerv | help;
	}

	TritSet::TritHelp& TritSet::TritHelp::operator=(Trit value) //перегрузка оператора присваивания
	{
		uint NeedUint = index / 4 / sizeof(uint);
		if (NeedUint > set.size_mass) {
			if (value == Unknown) {
				return *this;
			}
			uint *mass_support = (uint*)calloc(NeedUint + 1, sizeof(uint));
			memcpy(mass_support, set.mass, set.size_mass);
			free(set.mass);
			set.mass = mass_support;
			set.size_mass = NeedUint;
		}
		else {
			set.change(value, index);
		}
		return *this;
	}

	TritSet::TritHelp& TritSet::TritHelp::operator=(const TritSet::TritHelp &value) {
		set.change(value.set.znan(value.index), index);
		return *this;
	}

	std::ostream& operator <<(std::ostream& out, const TritSet::TritHelp &value) {
		out << value.set.znan(value.index);
		return out;
	}

	bool operator ==(const TritSet::TritHelp &set, Trit Value) {
		if (set.set.znan(set.index) == Value) {
			return true;
		}
		return false;
	}

	bool operator !=(const TritSet::TritHelp &set, Trit Value) {
		if (set.set.znan(set.index) != Value) {
			return true;
		}
		return false;
	}

	bool operator ==(Trit Value, const TritSet::TritHelp &set) {
		if (set.set.znan(set.index) == Value) {
			return true;
		}
		return false;
	}

	bool operator !=(Trit Value, const TritSet::TritHelp &set) {
		if (set.set.znan(set.index) != Value) {
			return true;
		}
		return false;
	}

	bool operator ==(const TritSet::TritHelp &set_one, const TritSet::TritHelp &set_two) {
		if (set_one.set.znan(set_one.index) == set_two.set.znan(set_two.index)) {
			return true;
		}
		return false;
	}

	bool operator !=(const TritSet::TritHelp &set_one, const TritSet::TritHelp &set_two) {
		if (&set_one == &set_two) {
			return false;
		}//поведение на случай если слева и справа один и тот же объект
		if (set_one.set.znan(set_one.index) != set_two.set.znan(set_two.index)) {
			return true;
		}
		return false;
	}


	TritSet::TritHelp TritSet::operator[](uint n) {
		return TritSet::TritHelp(*this, n);
	}

	void TritSet::shrink() {
		for (int i = size_mass - 1; i >= 0; --i) {
			if (mass[i] != 0) {
				uint *mass_support = (uint*)calloc(i + 1, sizeof(uint));
				memcpy(mass_support, mass, i + 1);
				free(mass);
				mass = mass_support;
				size_mass = i + 1;
				return;
			}
		}
		free(mass);
		mass = nullptr;
	}
	TritSet& TritSet::operator !() {
		uint index;
		for (uint i = 0; i < size_mass; ++i) {
			for (uint j = 0; j < sizeof(uint) * 4; ++j) {
				index = (i * 16) + j;
			}
			change(!znan(index), index);
		}
		return *this;
	}

	TritSet& TritSet::operator=(TritSet&& tmp) {
		mass = tmp.mass;
		size_mass = tmp.size_mass;
		tmp.size_mass = 0;
		tmp.mass = nullptr;
		return *this;
	}

	TritSet& TritSet::operator=(const TritSet& value) {
		size_mass = value.size_mass;
		free(mass);
		mass = (uint*)calloc(size_mass, sizeof(uint));
		memcpy(mass, value.mass, size_mass);
		return *this;
	}

	TritSet& TritSet::operator&=(TritSet &value) {
		if (this == &value) {
			return *this;
		}
		uint index;
		Trit left;
		Trit right;
		if (value.size_mass > size_mass) {
			uint new_size_mass = value.size_mass;
			uint *new_mass = (uint*)calloc(new_size_mass, sizeof(uint));
			memcpy(new_mass, mass, size_mass);
			free(mass);
			mass = new_mass;
			size_mass = new_size_mass;
		}
		for (uint i = 0; i < size_mass; ++i) {
			for (uint j = 0; j < sizeof(uint) * 4; ++j) {//sizeof(uint) * 8 / 4
				index = (i * 16) + j;
				left = znan(index);
				right = value.znan(index);
				change(left & right, index);
			}
		}
		return *this;
	}

	TritSet& TritSet::operator|=(TritSet &value) {
		if (this == &value) {
			return *this;
		}
		uint index;
		Trit left;
		Trit right;
		if (value.size_mass > size_mass) {
			uint new_size_mass = value.size_mass;
			uint *new_mass = (uint*)calloc(new_size_mass, sizeof(uint));
			memcpy(new_mass, mass, size_mass);
			free(mass);
			mass = new_mass;
			size_mass = new_size_mass;
		}
		for (uint i = 0; i < size_mass; ++i) {
			for (uint j = 0; j < sizeof(uint) * 4; ++j) {//sizeof(uint) * 8 / 4
				index = (i * 16) + j;
				left = znan(index);
				right = value.znan(index);
				change(left | right, index);
			}
		}
		return *this;
	}

	uint TritSet::capacity() {
		return size_mass;
	}

	uint TritSet::length() { //индекс последнего не Unknown трита + 1
		uint index;
		for (int i = size_mass - 1; i >= 0; --i) {
			if (mass[i] != 0) {
				for (int j = (sizeof(uint) * 4) - 1; j >= 0; --j) {//sizeof(uint) * 8 / 2
					index = (i * 16) + j;
					Trit sup = znan(index);
					if (sup != Unknown) {
						return (index + 1);
					}
				}
			}
		}
		return 0;
	}

	uint TritSet::cardinality(Trit value) {
		uint kolvo = 0;
		uint index;
		if (value == Unknown) {
			uint buffer = 0;
			for (uint i = 0; i < size_mass; ++i) {
				for (uint j = 0; j < sizeof(uint) * 4; ++j) {
					index = (i * 16) + j;
					if (znan(index) == value) {
						++buffer;
					}
					else {
						kolvo += buffer;
						buffer = 0;
					}
				}
			}
		}
		else {
			for (uint i = 0; i < size_mass; ++i) {
				for (uint j = 0; j < sizeof(uint) * 4; ++j) {
					index = (i * 16) + j;
					if (znan(index) == value) {
						++kolvo;
					}
				}
			}
		}
		return kolvo;
	}

	std::unordered_map<Trit, uint, std::hash<uint>> TritSet::cardinality() {
		std::unordered_map<Trit, uint, std::hash<uint>> hash(3);
		hash[Unknown] = cardinality(Unknown);
		hash[False] = cardinality(False);
		hash[True] = cardinality(True);
		return hash;
	}

	void TritSet::trim(uint LastIndex) {
		uint NeedUint = LastIndex / 4 / sizeof(uint);//в каком элементе массива трит
		if (NeedUint >= size_mass) {
			return;
		}
		uint NeedTritInBit = (LastIndex % (4 * sizeof(uint))) * 2;
		mass[NeedUint] = mass[NeedUint] << ((8 * sizeof(uint)) - NeedTritInBit);
			mass[NeedUint] = mass[NeedUint] >> ((8 * sizeof(uint)) - NeedTritInBit);
		for (uint i = NeedUint + 1; i < size_mass; ++i) {
			mass[i] = 0;
		}
	}

	TritSet operator &(TritSet &left, TritSet &right) {
		TritSet set;
		set = left;
		return set &= right;
	}

	TritSet operator |(TritSet &left, TritSet &right) {
		TritSet set;
		set = left;
		return set |= right;
	}
}

using namespace Trity;

TEST(TestTrity, testmemory) {
	//резерв памяти для хранения 1000 тритов
	TritSet set(1000);
	// length of internal array
	size_t allocLength = set.capacity();
	assert(allocLength >= 1000 * 2 / 8 / sizeof(uint));
	// 1000*2 - min bits count
	// 1000*2 / 8 - min bytes count
	// 1000*2 / 8 / sizeof(uint) - min uint[] size


	//не выделяет никакой памяти
	set[1000000000] = Unknown;
	assert(allocLength == set.capacity());


	// false, but no exception or memory allocation
	if (set[2000000000] == True) {}
	assert(allocLength == set.capacity());


	//выделение памяти
	set[1000000000] = True;
	assert(allocLength < set.capacity());


	//no memory operations
	allocLength = set.capacity();
	set[1000000000] = Unknown;
	set[1000000] = False;
	assert(allocLength == set.capacity());


	//освобождение памяти до начального значения или 
	//до значения необходимого для хранения последнего установленного трита
	//в данном случае для трита 1000’000
	set.shrink();
	assert(allocLength > set.capacity());
    TritSet setA(1000);
	TritSet setB(2000);
	TritSet setC = setA & setB;
	assert(setC.capacity() == setB.capacity());
	TritSet setD = setA | setB;
	assert(setD.capacity() == setB.capacity());
	TritSet setR(10);
	TritSet setM(30);
	setR &= setA;
	setM |= setA;
	assert(setR.capacity() == setA.capacity());
	assert(setM.capacity() == setA.capacity());
}

TEST(TestTrity, otherfunc) {
	TritSet set(40);

	set[14] = True;
	std::cout << set.length() << std::endl;
	assert(15 == set.length());
	set[7] = False;
	set[8] = False;
	assert(set.cardinality(False) == 2);
	assert(set.cardinality(Unknown) == 12);
	assert(set.cardinality(True) == 1);
	std::unordered_map<Trit, uint, std::hash<uint>> hash = set.cardinality();
	assert(hash[False] == 2);
	assert(hash[Unknown] == 12);
	assert(hash[True] == 1);
	set.trim(10);
	assert(set[14] == Unknown);
}
int main(int argc, char ** argv)
{


	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

