﻿#include <iostream>
#include <fstream>
#include "sort.h"

int main(int argc, char* argv[]) {
	if (argc != 3) {
		return 1;
	}
	list<string> mylist;
	string str;
	ifstream inp(argv[1]);//поток ввода
	if (!inp) {
		cout << "err" << endl;
		return 1;
	}
	ofstream fout(argv[2]);//поток вывода
	if (!fout) {
		cout << "err" << endl;
		return 1;
	}
	while (!inp.eof()) {//пока не конец файла пишем в контейнер
		getline(inp, str);
		mylist.push_front(str);
	}
	inp.close();//закрываем поток
	sort::sort_strings(mylist);//сортируем
	for (list<string>::iterator it = mylist.begin(); it != mylist.end(); ++it) {
		fout << *it << endl;
	}//пишем в файл
	fout.close();
	mylist.clear();
	return 0;
}