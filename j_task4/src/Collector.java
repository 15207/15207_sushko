import static java.lang.Thread.sleep;

public class Collector implements Work{
    private static int numberOfCar = 0;

    public void work(Factory f) {
        while(true) {
            Car newCar;
            newCar = new Car( f.bodyStock.get(),f.accessoryStock.get(),f.engineStock.get(),numberOfCar++);
            f.carStock.put(newCar);
            try {
                sleep(f.app.getSpeed_of_collectors());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}