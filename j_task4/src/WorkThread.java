class WorkThread extends Thread{
    private Work worker;
    private Factory factory;
    public WorkThread(Work worker, Factory factory) {
        this.worker = worker;
        this.factory = factory;
        this.start();
    }

    @Override
    public void run() {
        worker.work(factory);
    }
}