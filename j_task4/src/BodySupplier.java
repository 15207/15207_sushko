import static java.lang.Thread.sleep;

public class BodySupplier implements Work{
    static int numberOfBody = 0;
    public void work(Factory f){
        while(true) {
            try {
                System.out.println("i'm body and i will sleep for " + f.app.getSpeed_of_body_suppliers());
                sleep(f.app.getSpeed_of_body_suppliers());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Body newBody = new Body(numberOfBody++);
            f.bodyStock.put(newBody);
            f.app.setNumber_of_bodies(f.bodyStock.get_size());
        }
    }
}