import static java.lang.Thread.sleep;

public class EngineSupplier implements Work{
    static int numberOfEngine = 0;
    public void work(Factory f) {
        while (true) {
            try {
                System.out.println("i'm engine and i will sleep for " + f.app.getSpeed_of_engine_suppliers());
                sleep(f.app.getSpeed_of_engine_suppliers());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Engine newEngine = new Engine(numberOfEngine++);
            f.engineStock.put(newEngine);
            f.app.setNumber_of_engines(f.engineStock.get_size());
        }
    }
}