
class MainClass{
    public static void main(String[] args) {
        Factory factory = new Factory();
        try {
            factory.start();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
}