import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class SimpleGUI extends JFrame {
    private JLabel engine_lable = new JLabel("Engine Stock ");
    private JLabel number_of_engines = new JLabel("0");
    private JLabel body_lable = new JLabel("Body Stock ");
    private JLabel number_of_bodies = new JLabel("0");
    private JLabel accessory_label = new JLabel("Accessory Stock ");
    private JLabel number_of_accessories = new JLabel("0");
    private JLabel engine_supplier = new JLabel("    Engine Supplier ");
    private JLabel accessories_supplier = new JLabel("Accessories supplier ");
    private JLabel dealer = new JLabel("              Dealer ");
    private JLabel body_supplier = new JLabel("    Body Supplier ");
    private JLabel collector = new JLabel("           Collector ");
    private JSlider speed_of_collectors = new JSlider(JSlider.VERTICAL,0,1000,200);
    private JSlider speed_of_body_suppliers = new JSlider(JSlider.VERTICAL,0,1000,200);
    private JSlider speed_of_engine_suppliers = new JSlider(JSlider.VERTICAL,0,1000,200);
    private JSlider speed_of_accessors_suppliers = new JSlider(JSlider.VERTICAL,0,1000,200);
    private JSlider speed_of_dealers = new JSlider(JSlider.VERTICAL,0,1000,200);

    public void setNumber_of_engines(Integer label) {
        number_of_engines.setText(label.toString());
    }

    public void setNumber_of_bodies(Integer label) {
        number_of_bodies.setText(label.toString());
    }

    public void setNumber_of_accessories(Integer label) {
        number_of_accessories.setText(label.toString());
    }

    public int getSpeed_of_collectors() {
        return speed_of_collectors.getValue();
    }

    public int getSpeed_of_body_suppliers() {
        return speed_of_body_suppliers.getValue();
    }

    public int getSpeed_of_engine_suppliers() {
        return speed_of_engine_suppliers.getValue();
    }

    public int getSpeed_of_accessors_suppliers() {
        return speed_of_accessors_suppliers.getValue();
    }

    public int getSpeed_of_dealers() {
        return speed_of_dealers.getValue();
    }

    public SimpleGUI(OutputStreamWriter fout) {
        super("Simple Example");
        this.setBounds(0, 0, 800, 500);
        WindowListener exitListener = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                if (fout!=null){
                    try {
                        fout.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                System.exit(0);
            }
        };
        this.addWindowListener(exitListener);
        this.setVisible(true);
        JPanel container = new JPanel();
        JPanel container2 = new JPanel();
        JPanel container3 = new JPanel();
        container.setLayout(new GridLayout(1,6));
        container2.setLayout(new GridLayout(1,5));
        container3.setLayout(new GridLayout(1,5));
        container.add(engine_lable);
        container.add(number_of_engines);
        container.add(body_lable);
        container.add(number_of_bodies);
        container.add(accessory_label);
        container.add(number_of_accessories);
        container2.add(speed_of_collectors);
        container2.add(speed_of_body_suppliers);
        container2.add(speed_of_engine_suppliers);
        container2.add(speed_of_accessors_suppliers);
        container2.add(speed_of_dealers);
        container3.add(collector);
        container3.add(body_supplier);
        container3.add(engine_supplier);
        container3.add(accessories_supplier);
        container3.add(dealer);
        this.getContentPane().add(container, BorderLayout.NORTH);
        this.getContentPane().add(container2, BorderLayout.CENTER);
        this.getContentPane().add(container3, BorderLayout.SOUTH);
    }
}