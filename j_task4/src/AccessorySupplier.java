import static java.lang.Thread.sleep;

public class AccessorySupplier implements Work{
    static int numberOfAccessory = 0;
    public void work(Factory f)  {
        while (true) {
            try {
                System.out.println("i'm accessor and i will sleep for " + f.app.getSpeed_of_accessors_suppliers());
                sleep(f.app.getSpeed_of_accessors_suppliers());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Accessory newAccessory = new Accessory(numberOfAccessory++);
            f.accessoryStock.put(newAccessory);
            f.app.setNumber_of_accessories(f.accessoryStock.get_size());
        }
    }
}