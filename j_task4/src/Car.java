class Car{
    Body body;
    Accessory accessory;
    Engine engine;
    int ID;

    public Body getBody() {
        return body;
    }

    public Accessory getAccessory() {return accessory;}

    public Engine getEngine() {
        return engine;
    }

    public Car(Body body, Accessory accessory, Engine engine, int ID) {
        this.body = body;
        this.accessory = accessory;
        this.engine = engine;
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }
}