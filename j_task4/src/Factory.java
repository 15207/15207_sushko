import java.io.*;

public class Factory {
    Stock<Body> bodyStock;
    Stock<Car> carStock;
    Stock<Engine> engineStock;
    Stock<Accessory> accessoryStock;
    SimpleGUI app;
    Config conf;
    Factory (){
        try(FileInputStream fin = new FileInputStream("resource/config.properties")) {
            this.conf = new Config(fin);
        }
        catch (IOException ex){
            System.out.println("No config file");
            System.exit(1);
        }
        accessoryStock = new Stock<>(conf.getStorageAccessorySize());
        engineStock = new Stock<>(conf.getStorageEngineSize());
        carStock = new Stock<>(conf.getStorageCarSize());
        bodyStock = new Stock<>(conf.getStorageBodySize());
    }
    void start() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        OutputStreamWriter fout = null;
        if (conf.isLogSale()) {
            try {
                fout = new FileWriter("log.txt");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        app = new SimpleGUI(fout);
        AccessorySupplier[] accessorySuppliers = new AccessorySupplier[conf.getAccessorySuppliers()];
        Work bodySupplier = new BodySupplier();
        new Thread(new WorkThread(bodySupplier,this));
        Work engineSupplier = new EngineSupplier();
        new Thread(new WorkThread(engineSupplier,this));
        for (int i = 0; i < conf.getAccessorySuppliers(); i++){
            accessorySuppliers[i] = new AccessorySupplier();
            new Thread(new WorkThread(accessorySuppliers[i], this));
        }
        Work[] collectors = new Collector[conf.getCollectors()];
        for (int i = 0; i < conf.getCollectors(); i++){
            collectors[i] = new Collector();
            new Thread(new WorkThread(collectors[i],this));
        }
        Dealer[] dealers = new Dealer[conf.getDealers()];
        if (conf.isLogSale()) {
            for (int i = 0; i < conf.getDealers(); i++) {
                dealers[i] = new Dealer(i, fout);
                new Thread(new WorkThread(dealers[i], this));
            }
        }
        else {
            for (int i = 0; i < conf.getDealers(); i++) {
                dealers[i] = new Dealer(i);
                new Thread(new WorkThread(dealers[i], this));
            }
        }

        }

    public Boolean getLog() {
        return conf.isLogSale();
    }
}