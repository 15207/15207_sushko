import java.util.Vector;

class Stock<T>{
    private Vector<T> arr;
    public Stock(int size) {
        arr = new Vector<>(size);
    }
    public synchronized void put(T detail){
        while (this.isFull()){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.arr.add(detail);
        notify();
    }
    public synchronized T get(){
        while (arr.isEmpty()){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        notify();
        T tmp = arr.firstElement();
        this.arr.remove(arr.firstElement());
        return tmp;
    }
    public boolean isFull(){return arr.size()==arr.capacity();}
    public int get_size(){
        return arr.size();
    }
}