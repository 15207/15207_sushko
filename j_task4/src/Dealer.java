import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;
import java.text.*;

import static java.lang.Thread.sleep;

public class Dealer implements Work {
    private int ID;
    private OutputStreamWriter fout;
    public Dealer(int ID, OutputStreamWriter fout) {
        this.ID = ID;
        this.fout = fout;
    }
    public Dealer(int ID) {
        this.ID = ID;
    }

    public void work(Factory f){
        Car tmp;
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss");
        try{
            while(true){
                tmp = f.carStock.get();
                if (f.getLog()){
                    Date date = new Date();
                    if (fout !=null) {
                        fout.write(formatForDateNow.format(date) + ":Dealer<" + this.ID + ">:Auto<" + tmp.getID() + ">(Body:<" + tmp.getBody().getID() + ">,Engine:<" + tmp.getEngine().getID() + ">,Accessory:<" + tmp.getAccessory().getID() + ">)\n");
                        fout.flush();
                    }
                }
                try {
                    sleep(f.app.getSpeed_of_dealers());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}