package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.CalcError;
import CalculatorPack.Context;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Archangel on 28.04.2017.
 */
public class CommandPushTest {
    @Test
    public void execute() throws Exception {
        Context context = new Context();
        Command cmd = new CommandPush();
        List<String> arg = new ArrayList<String>();
        arg.add("10");
        try
        {
            cmd.execute(arg,context,null);
        }
        catch (CalcError e) {}
        Assert.assertEquals(10,context.popStack(),0);
    }

}