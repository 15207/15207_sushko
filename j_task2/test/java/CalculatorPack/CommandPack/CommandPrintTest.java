package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.CalcError;
import CalculatorPack.Context;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * Created by Archangel on 28.04.2017.
 */
public class CommandPrintTest {
    @Test
    public void execute() throws Exception {
        Context context = new Context();
        context.pushStack(5.0);
        Command cmd = new CommandPrint();
        OutputStream out = new ByteArrayOutputStream();
        PrintStream str = new PrintStream(out);

        try
        {
            cmd.execute(null,context,out);
        }
        catch (CalcError e) {}
        OutputStream out2 = new ByteArrayOutputStream();
        str = new PrintStream(out2);
        str.println("5.0");
        Assert.assertEquals(out2.toString(), out.toString());
    }

}