package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.CalcError;
import CalculatorPack.Context;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Archangel on 28.04.2017.
 */
public class CommandMinusTest {
    @Test
    public void execute() throws Exception {
        Context context = new Context();
        Command cmd = new CommandMinus();
        context.pushStack(3);
        context.pushStack(10);
        try
        {
            cmd.execute(null,context,null);
        }
        catch (CalcError e) {}
        Assert.assertEquals(7,context.popStack(),0);
    }
}