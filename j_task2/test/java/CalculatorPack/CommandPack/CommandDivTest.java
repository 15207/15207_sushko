package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.CalcError;
import CalculatorPack.Context;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Archangel on 28.04.2017.
 */
public class CommandDivTest {
    @Test
    public void execute() throws Exception {
            Context context = new Context();
            Command cmd = new CommandDiv();
            context.pushStack(5);
            context.pushStack(15);
            try
            {
                cmd.execute(null,context,null);
            }
            catch (CalcError e) {}
            Assert.assertEquals(3,context.popStack(),0);
        }

    }