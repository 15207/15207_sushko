package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.CalcError;
import CalculatorPack.CalculatorError.DefineError;
import CalculatorPack.CalculatorError.ErrorKolvoArg;
import CalculatorPack.Context;
import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Archangel on 28.04.2017.
 */
public class CommandDefineTest{
    @Test
    public void execute() throws Exception {
        Context context = new Context();
        Command cmd = new CommandDefine();
        List<String> args = new ArrayList<String>();
        args.add("Aang");
        args.add("45");
        try
        {
            cmd.execute(args,context,null);
        }
        catch (CalcError e) {}
        Assert.assertEquals(45, context.getMap("Any"),0);
    }

}