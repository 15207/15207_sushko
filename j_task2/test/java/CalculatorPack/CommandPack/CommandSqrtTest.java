package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.CalcError;
import CalculatorPack.Context;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Archangel on 28.04.2017.
 */
public class CommandSqrtTest {
    @Test
    public void execute() throws Exception {
        Context context = new Context();
        Command cmd = new CommandSqrt();
        context.pushStack(81);
        try
        {
            cmd.execute(null,context,null);
        }
        catch (CalcError ex) {}
        Assert.assertEquals(9,context.popStack(),0);
    }
}





