package CalculatorPack;

import CalculatorPack.CalculatorError.CalcError;
import CalculatorPack.CalculatorError.CommandError;
import CalculatorPack.CalculatorError.ErrProperties;
import CalculatorPack.CommandPack.Command;
import CalculatorPack.CommandPack.CommandFactory;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;

/**
 * Created by Archangel on 10.03.2017.
 */

public class Calculator {
    Context context;
    private static Logger log = Logger.getLogger(Calculator.class.getName());
    public void run(InputStream input, OutputStream output){
        String buffer;
        context = new Context();
        Scanner scan = new Scanner(input);
        int numbLine = 1;
        while (scan.hasNextLine()) {
            try {
            buffer = scan.nextLine();
            if (buffer.length() == 0){
                return;
            }
            if(buffer.charAt(0) == '#'){
                continue;
            }
            List<String> commandLine = parseLine(buffer);
            log.info("Command is succesfully parsed, line: " +numbLine + "\n");
            String commandName = commandLine.get(0);
            commandLine.remove(0);
            Command cmd;

                cmd = CommandFactory.getInstance().getCommand(commandName);
                log.info("Command class is succesfully created by the factory.");
                cmd.execute(commandLine, context, output);
                log.info("Command is succesfully performed.");
            }
            catch (ErrProperties e) {
                e.printErr(numbLine);
                return;
            }
            catch (CalcError err){
                err.printErr(numbLine);
            }

            ++numbLine;
        }
    }

    private List<String> parseLine(String val){
        Scanner scan = new Scanner(val);
        List<String> com = new LinkedList<String>();
        while(scan.hasNext())
            com.add(scan.next());
        return com;
    }
}
