package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.DefineError;
import CalculatorPack.CalculatorError.ErrorKolvoArg;
import CalculatorPack.Context;

import java.util.List;
import java.io.OutputStream;
/**
 * Created by Archangel on 12.03.2017.
 */
public class CommandDefine implements Command{

    public void execute(List<String> commandLine, Context context, OutputStream output) throws DefineError, ErrorKolvoArg{
        if (commandLine.size() == 0 || commandLine.size() > 2){
            throw new ErrorKolvoArg();
        }
        try {
            context.setMap(commandLine.get(0), Double.parseDouble(commandLine.get(1)));
        } catch(NumberFormatException e){
            throw new DefineError();
        }
    }
}
