package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.ErrorKolvoArg;
import CalculatorPack.CalculatorError.NumbError;
import CalculatorPack.Context;

import java.util.List;
import java.io.OutputStream;
/**
 * Created by Archangel on 12.03.2017.
 */
public class CommandPush implements Command{
    public void execute(List<String> commandLine, Context context, OutputStream output) throws NumbError, ErrorKolvoArg {
        if (commandLine.size() != 1){
            throw new ErrorKolvoArg();
        }
        if (context.getMap(commandLine.get(0)) != null) {
            context.pushStack(context.getMap(commandLine.get(0)));
        } else {
            try {
                context.pushStack(Double.parseDouble(commandLine.get(0)));
            }catch (NumberFormatException e) {
                throw new NumbError();
            }
        }
    }
}
