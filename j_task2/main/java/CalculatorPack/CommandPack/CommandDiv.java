package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.EmtryStackError;
import CalculatorPack.Context;

import java.util.EmptyStackException;
import java.util.List;
import java.io.OutputStream;
import java.util.NoSuchElementException;

/**
 * Created by Archangel on 12.03.2017.
 */
public class CommandDiv implements Command{
    public void execute(List<String> commandLine, Context context, OutputStream output) throws EmtryStackError {
        try {
            if (context.sizeStack() < 2){
                throw new EmptyStackException();
            }
            double one = context.popStack();
            double two = context.popStack();
            context.pushStack(two/one);
        } catch (EmptyStackException e){
            throw new EmtryStackError();
        }
    }
}
