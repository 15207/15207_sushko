package CalculatorPack.CommandPack;

import CalculatorPack.Calculator;
import CalculatorPack.CalculatorError.CommandError;
import CalculatorPack.CalculatorError.ErrProperties;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Created by Archangel on 12.03.2017.
 */
public class CommandFactory {
    private static Properties properties = null;
    private static HashMap<String, Class<Command>> classes = null;
    private static CommandFactory conf = null;
    private static Logger log = Logger.getLogger(Calculator.class.getName());

    public static CommandFactory getInstance() throws ErrProperties
    {
        if(conf ==null)
        {
            conf = new CommandFactory();
        }
        return conf;
    }

    private CommandFactory() throws ErrProperties
    {
        classes = new HashMap<String, Class<Command>>();
        InputStream str = CommandFactory.class.getResourceAsStream("/CalculatorPack/Resourse/CommandFactory.properties");
        try
        {
            properties = new Properties();
            properties.load(str);
            str.close();
        } catch (IOException e) {
            throw new ErrProperties();
        }
        log.info("Config-file for IoC (CommandFactory) is read succesfully.");
    }

    public static Command getCommand(String nameCommand) throws CommandError {
        Command cmd = null;
        Class<?> cls = null;

        String key = properties.getProperty(nameCommand);
        if (key == null) {
            throw new CommandError();
        }
        log.info("IoC is loaded succecfully.");
        try {
            if (!classes.containsKey(key)) {
                cls = Class.forName(key);       //ClassNotFoundException
                classes.put(nameCommand, (Class<Command>) cls);
            } else {
                cls = classes.get(nameCommand);
            }
            cmd = (Command) cls.newInstance();
            log.info("Command class " + cls.getName() + " is created.");
            return cmd;
        } catch (Exception e) {
            throw new CommandError();
        }
    }
}

