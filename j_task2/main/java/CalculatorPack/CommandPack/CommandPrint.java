package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.EmtryStackError;
import CalculatorPack.Context;

import java.io.PrintWriter;
import java.util.EmptyStackException;
import java.util.List;
import java.io.OutputStream;

/**
 * Created by Archangel on 12.03.2017.
 */
public class CommandPrint implements Command{
    public void execute(List<String> commandLine, Context context, OutputStream output) throws EmtryStackError {
        try {
            PrintWriter oos = new PrintWriter(output, true);
            oos.println(context.peekStack());
        }
            catch (EmptyStackException j){
            throw new EmtryStackError();
        }
    }
}
