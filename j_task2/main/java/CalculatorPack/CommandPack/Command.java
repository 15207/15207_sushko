package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.CalcError;
import CalculatorPack.Context;

import java.util.List;
import java.io.OutputStream;
/**
 * Created by Archangel on 12.03.2017.
 */
public interface Command {
    public void execute(List<String> commandLine, Context context, OutputStream output) throws CalcError;
}
