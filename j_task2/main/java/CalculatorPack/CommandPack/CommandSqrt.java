package CalculatorPack.CommandPack;

import CalculatorPack.CalculatorError.EmtryStackError;
import CalculatorPack.Context;

import java.util.EmptyStackException;
import java.util.List;
import java.io.OutputStream;
/**
 * Created by Archangel on 12.03.2017.
 */
public class CommandSqrt implements Command{
    public void execute(List<String> commandLine, Context context, OutputStream output) throws EmtryStackError {
        try {
            double temp = context.popStack();
            temp = Math.sqrt(temp);
            context.pushStack(temp);
        } catch (EmptyStackException e){
            throw new EmtryStackError();
        }
    }
}
