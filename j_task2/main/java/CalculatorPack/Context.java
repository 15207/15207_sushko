package CalculatorPack;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * Created by Archangel on 12.03.2017.
 */
public class Context {
    private Stack<Double> stack;
    private HashMap<String, Double> map;
    public Context(){
        stack = new Stack<Double>();
        map = new HashMap<String, Double>();
    }
    public void pushStack(double numb){
        stack.push(numb);
    }
    public Double popStack() throws EmptyStackException{
        return stack.pop();
    }
    public Double peekStack() throws EmptyStackException{
        return stack.pop();
    }
    public int sizeStack(){
        return stack.size();
    }
    public void setMap(String key, double value){
        map.put(key, value);
    }
    public Double getMap(String key) throws NoSuchElementException{
        return map.get(key);
    }
}
