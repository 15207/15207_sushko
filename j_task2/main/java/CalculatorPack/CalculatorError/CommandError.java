package CalculatorPack.CalculatorError;

/**
 * Created by Archangel on 12.03.2017.
 */
public class CommandError extends CalcError {
        public void printErr (int numbLine){
            System.err.println("Unknown command. Line: " + numbLine + '\n');
        }
}
