package CalculatorPack.CalculatorError;

/**
 * Created by Archangel on 26.03.2017.
 */
public class DefineError extends CalcError {
    public void printErr (int numbLine){
        System.err.println("Define error. Line: " + numbLine + '\n');
    }
}
