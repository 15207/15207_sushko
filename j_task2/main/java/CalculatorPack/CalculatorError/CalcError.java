package CalculatorPack.CalculatorError;

/**
 * Created by Archangel on 12.03.2017.
 */

public abstract class CalcError extends Exception {
    public abstract void printErr (int numbLine);
}
