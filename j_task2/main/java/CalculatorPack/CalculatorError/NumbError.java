package CalculatorPack.CalculatorError;

/**
 * Created by Archangel on 12.03.2017.
 */
public class NumbError extends CalcError {
    @Override
    public void printErr(int numbLine) {
        System.err.println("NumberFormat error. Line: " + numbLine + '\n');
    }
}
