import CalculatorPack.Calculator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by Archangel on 10.03.2017.
 */
public class Mymain {
    public static void main(String[] args) {
        FileInputStream file;
        try {
             file = new FileInputStream(args[0]);
        } catch (FileNotFoundException e){
            System.out.println("not found file");
            return;
        }
        Calculator calk = new Calculator();
        calk.run(System.in, System.out);
    }
}
