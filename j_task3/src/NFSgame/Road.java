package NFSgame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Archangel on 28.04.2017.
 */
public class Road extends JPanel implements ActionListener{
    private Image image;
    private Player gamer;
    private Timer timer;
    private Thread enemiesFactory;
    private Thread audioThread;
    private ArrayList<Enemy> enemyList = new ArrayList<Enemy>();
    private int width;
    private int height;
    private AudioThread audi;
    public Road(){
        image = new ImageIcon(getClass().getClassLoader().getResource("ResFiles/Pictures/doroga.png")).getImage();
        width = image.getWidth(this);
        height = image.getHeight(this);
        gamer = new Player(this);
        timer = new Timer(20, this);
        addKeyListener(new myKeyboard());
        setFocusable(true);
        enemiesFactory = new Thread(new EnemiesFactory(this, enemyList));
        audi =  new AudioThread(this);
        audioThread = new Thread(audi);
        enemiesFactory.start();
        audioThread.start();
        timer.start();
    }

    @Override
    public int getWidth() {
        return width;
    }
    @Override
    public int getHeight() {
        return height;
    }
    public Image getImage() {
        return image;
    }

    private class myKeyboard extends KeyAdapter{
        @Override
        public void keyPressed(KeyEvent e) {
           gamer.keyPressed(e);
        }

        @Override
        public void keyReleased(KeyEvent e) {
            gamer.keyReleased(e);
        }
    }

    public void paint(Graphics g){
        g = (Graphics2D) g;
        g.drawImage(image, gamer.getFrame_shift1(),0,null);
        g.drawImage(image, gamer.getFrame_shift2(),0,null);
        g.drawImage(gamer.getImage(), gamer.getX(), gamer.getY(),null);
        Iterator<Enemy> iterator=enemyList.iterator();
        while (iterator.hasNext()){
            Enemy e = iterator.next();
            if( e.x>=2400 || e.x <=-2400){
                iterator.remove();
            } else {
                g.drawImage(e.image, e.x, e.y, null);
            }
        }

        double speed = 200*gamer.getSpeed()/Player.MAX_SPEED;
        g.setColor(Color.BLACK);
        Font font = new Font("Arial", Font.ITALIC, 20);
        g.setFont(font);
        g.drawString("Speed: " + speed + " km/h", width / 10, (height / 20));
        g.drawString("Point: " + gamer.getPath(), (width * 9 / 10), (height / 20));
    }
    public void actionPerformed(ActionEvent ev){
        gamer.move();
        Iterator<Enemy> iterator=enemyList.iterator();
        while (iterator.hasNext()){
            Enemy e=iterator.next();
            e.move();
        }
        checkCrashWithEnemies();
        repaint();
    }

    private void checkCrashWithEnemies() {
        Iterator<Enemy> iterator=enemyList.iterator();
        while (iterator.hasNext()){
            Enemy e=iterator.next();
            if(gamer.getRectangle().intersects(e.getRectangle())){
                audioThread.interrupt();
                audi.setGameOver();
                audioThread = new Thread(audi);
                JOptionPane.showMessageDialog(null, "You died");
            }

        }
    }

    public Player getPlayer(){
        return gamer;
    }
}
