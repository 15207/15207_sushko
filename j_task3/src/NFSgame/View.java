package NFSgame;

import sun.awt.Mutex;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Iterator;

/**
 * Created by Archangel on 01.06.2017.
 */

public class View extends JPanel {
    private RoadModel road;
    private JFrame frame;
    private JMenuBar jMenuBar = new JMenuBar();
    private JMenu game = new JMenu("Game");
    private JMenuItem newGame = new JMenuItem("new game");
    private JMenuItem exit = new JMenuItem("exit");
    private Image image;
    private Image playerImage_center;
    private Image playerImage;
    private Image playerImage_top;
    private Image  playerImage_down;
    private Image imageEnemy;
    public View(RoadModel road, Controller controller){
        this.road = road;
        setFocusable(true);
        frame = new JFrame("Speedster");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1680, 1080);
        frame.add(this);
        image = new ImageIcon(getClass().getClassLoader().getResource("ResFiles/Pictures/doroga.png")).getImage();
        playerImage_center= new ImageIcon(getClass().getClassLoader().getResource("ResFiles/Pictures/Player.png")).getImage();
        playerImage = playerImage_center;
        playerImage_top = new ImageIcon(getClass().getClassLoader().getResource("ResFiles/Pictures/Player_Top.png")).getImage();
        playerImage_down = new ImageIcon(getClass().getClassLoader().getResource("ResFiles/Pictures/Player_Down.png")).getImage();
        imageEnemy = new ImageIcon(getClass().getClassLoader().getResource("ResFiles/Pictures/Enemey.png")).getImage();
        jMenuBar.add(game);
        game.add(newGame);
        newGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                   road.repeatGame();
            }
        });
        game.add(exit);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        frame.setJMenuBar(jMenuBar);
        this.road.setParametersLocation( image.getWidth(this),  image.getHeight(this), //эти параметры определяются реализацией
                playerImage.getWidth(this), playerImage.getHeight(this),
                imageEnemy.getHeight(this), imageEnemy.getWidth(this));
        addKeyListener(controller.getMyKeyboard());
        frame.setVisible(true);
    }

    public void setTopImage(){
        playerImage = playerImage_top;
    }
    public void setDownImage(){
        playerImage = playerImage_down;
    }
    public void setCenterImage(){
        playerImage = playerImage_center;
    }
    public void paint(Graphics g){
        g = (Graphics2D) g;
        g.drawImage(image, road.getPlayer().getFrame_shift1(),0,null);
        g.drawImage(image, road.getPlayer().getFrame_shift2(),0,null);
        g.drawImage(playerImage, road.getPlayer().getX(), road.getPlayer().getY(),null);
        synchronized (road.getEnemyList()) {
            Iterator<Enemy> iterator = road.getEnemyList().iterator();
            while (iterator.hasNext()) {
                    Enemy e = iterator.next();
                    if (e.x >= 2400 || e.x <= -2400) {
                        iterator.remove();
                    } else {
                        g.drawImage(imageEnemy, e.x, e.y, null);
                    }
                }
        }
        double speed = 200*road.getPlayer().getSpeed()/Player.MAX_SPEED;
        g.setColor(Color.BLACK);
        Font font = new Font("Arial", Font.ITALIC, 20);
        g.setFont(font);
        g.drawString("Speed: " + speed + " km/h", road.getWidth() / 10, (road.getHeight() / 20));
        g.drawString("Point: " + road.getPlayer().getPath(), (road.getWidth() * 9 / 10), (road.getHeight() / 20));
    }
}
