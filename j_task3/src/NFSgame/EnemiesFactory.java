package NFSgame;

import sun.awt.Mutex;

import java.awt.*;
import java.util.List;
import java.util.Random;

/**
 * Created by Archangel on 01.05.2017.
 */
public class EnemiesFactory implements Runnable {
    RoadModel roadModel;
    List list;
    public EnemiesFactory(RoadModel roadModel){
        this.roadModel = roadModel;
        this.list = roadModel.getEnemyList();
    }
    @Override
    public void run() {
        while(true){
            try {
                Random rand = new Random();
                Thread.sleep(rand.nextInt(2000));
                if(!roadModel.isGameOver()) {
                    synchronized (list) {
                        list.add(new Enemy(roadModel.getWidth(),
                                (rand.nextInt(roadModel.getHeight() * 9 / 10) + (roadModel.getHeight() / 50)),
                                rand.nextInt((int) (roadModel.getPlayer().MAX_SPEED * 0.75)),
                                roadModel, roadModel.getHeightEnemies(), roadModel.getWidthEnemies()));
                    }
                }
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
    }
}

