package NFSgame;

import java.awt.*;

/**
 * Created by Archangel on 28.04.2017.
 */
abstract public class Car {
    protected int x;
    protected int y;
    protected int speed;
    protected RoadModel roadModel;
    protected int width;
    protected int height;
    protected int max_top;
    protected int max_down;
    abstract public void move();
    public Rectangle getRectangle(){
        return new Rectangle(x, y, width,height);
    }
}
