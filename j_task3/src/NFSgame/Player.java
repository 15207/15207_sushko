package NFSgame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Created by Archangel on 28.04.2017.
 */
public class Player extends Car{

    private int difSpeed=0;
    private long path;
    private int frame_shift1;
    private int frame_shift2;
    private int difY = 0;
    public static final int MAX_SPEED = 100;
    public Player(RoadModel roadModel){
        this.roadModel = roadModel;
    }
    public void setParameters(int width, int height){
        this.width=width;
        this.height=height;
        startPozition();
        max_top = roadModel.getHeight() / 10;
        max_down = roadModel.getHeight() * 9 / 10;

    }
    public void startPozition(){
        frame_shift1 = 0;
        frame_shift2 = roadModel.getWidth();
        x = frame_shift2 / 50;
        y = max_top + 1;
        speed = 0;
        path = 0;
    }
    public long getPath() {
        return path;
    }
    public int getX(){
        return x;
    }
    public int getY(){
        return y;
    }
    public int getFrame_shift1(){
        return frame_shift1;
    }
    public int getFrame_shift2() {
        return frame_shift2;
    }
    public int getSpeed() {
        return speed;
    }
    public void keyPressed(KeyEvent e, View view) {
        int key=e.getKeyCode();
        if(key == KeyEvent.VK_RIGHT){
            difSpeed=MAX_SPEED / 50;
        }
        if(key == KeyEvent.VK_LEFT){
            difSpeed=-MAX_SPEED / 50;
        }
        if (key == KeyEvent.VK_UP){
            if (y <=max_down){
                difY = max_down / 30;
            }
            view.setTopImage();
        }
        if (key == KeyEvent.VK_DOWN){
            if (y >=max_top){
                difY = - max_down / 30;
            }
            view.setDownImage();
        }
    }
    public void keyReleased(KeyEvent e, View view) {
        int key=e.getKeyCode();
        if(key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_LEFT){
            difSpeed=0;
        }
        if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN){
            difY = 0;
            view.setCenterImage();
        }
    }
    public void move(){
        path += speed;
        speed += difSpeed;
        if (speed <= 0) {
            speed = 0;
        }
        if (speed > MAX_SPEED){
            speed = MAX_SPEED;
        }
        y -=difY;
        if (y <= max_top){
            y= max_top;
        }
        if (y >= max_down){
            y= max_down;
        }
        if(frame_shift2 - speed <=0){
            frame_shift1 =0;
            frame_shift2 = roadModel.getWidth();
        }else{

           frame_shift1 -=speed;
           frame_shift2 -=speed;
        }
    }
}
