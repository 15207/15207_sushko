package NFSgame;

import  javazoom.jl.player.Player;
import  javazoom.jl.decoder.JavaLayerException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by Archangel on 01.05.2017.
 */
public class AudioThread implements Runnable {
    RoadModel roadModel;
    Player player;

    public AudioThread(RoadModel roadModel) {
        this.roadModel = roadModel;
        setGameStart();
    }

    public void setGameStart() {
        try {
            player = new Player(new FileInputStream(getClass().getClassLoader().getResource("ResFiles/Audio/Erondon.mp3").getPath()));
        } catch (FileNotFoundException | JavaLayerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            player.play();
        } catch (JavaLayerException r) {
            r.printStackTrace();
        }
    }
}
