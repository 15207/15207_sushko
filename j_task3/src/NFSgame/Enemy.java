package NFSgame;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Archangel on 28.04.2017.
 */
public class Enemy extends Car{
     public Enemy(int x, int y, int speed, RoadModel roadModel, int height, int width){
         this.x=x;
         this.y=y;
         this.speed=speed;
         this.roadModel = roadModel;
         this.height = height;
         this.width = width;
     }
     public void move(){
         x=x- roadModel.getPlayer().speed + speed;
     }
}
