package NFSgame;

import sun.awt.Mutex;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Archangel on 28.04.2017.
 */
public class RoadModel {
    private Player gamer;
    private Thread enemiesFactory;
    private Thread audioThread;
    private ArrayList<Enemy> enemyList = new ArrayList<Enemy>();
    private int width;
    private int height;
    private int widthEnemies;
    private int heightEnemies;
    private AudioThread audi;
    private boolean isOver = false;
    public RoadModel(){
        gamer = new Player(this);
        audi =  new AudioThread(this);
        audioThread = new Thread(audi);
        audioThread.start();
    }
    public void setParametersLocation(int width, int height, int playerWidth, int playerHeight, int enemyWidth, int enemyHeight){
        this.width =width;
        this.height=height;
        gamer.setParameters(playerWidth, playerHeight);
        this.widthEnemies = enemyWidth;
        this.heightEnemies = enemyHeight;
        enemiesFactory = new Thread(new EnemiesFactory(this));
        enemiesFactory.start();
    }


    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
    public int getHeightEnemies() {
        return heightEnemies;
    }
    public int getWidthEnemies() {
        return widthEnemies;
    }
    public ArrayList<Enemy> getEnemyList() {
        return enemyList;
    }
    public void checkCrashWithEnemies() {
            synchronized (enemyList) {
            Iterator<Enemy> iterator = enemyList.iterator();
            while (iterator.hasNext()) {
                    Enemy e = iterator.next();
                    if (gamer.getRectangle().intersects(e.getRectangle())) {
                       // audioThread.stop();
                        isOver = true;
                        enemyList.clear();
                        JOptionPane.showMessageDialog(null, "You died");
                    }
                }
            }

    }
    public boolean isGameOver(){
        return isOver;
    }
    public void repeatGame(){
        gamer.startPozition();
        isOver = false;
       // audi =  new AudioThread(this);
       // audioThread = new Thread(audi);
      //  audioThread.start();
    }
    public void moveEnemies(){
        synchronized (enemyList) {
            Iterator<Enemy> iterator = enemyList.iterator();
            while (iterator.hasNext()) {
                Enemy e = iterator.next();
                e.move();
            }
        }
    }


    public Player getPlayer(){
        return gamer;
    }
}
