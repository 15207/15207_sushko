package NFSgame;

import sun.awt.Mutex;

import javax.swing.*;
import java.awt.event.*;

/**
 * Created by Archangel on 01.06.2017.
 */
public class Controller  implements ActionListener {
    RoadModel roadModel;
    Timer timer;
    View view;
    public Controller() {
        roadModel = new RoadModel();
        view = new View(roadModel, this);
        timer = new Timer(20, this);
        timer.start();
    }
    public void actionPerformed(ActionEvent ev){
        if (!roadModel.isGameOver()) {
            roadModel.getPlayer().move();
            roadModel.moveEnemies();
            roadModel.checkCrashWithEnemies();
            view.repaint();
        }
    }

    private class myKeyboard extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            roadModel.getPlayer().keyPressed(e, view);
        }
        @Override
        public void keyReleased(KeyEvent e) {
            roadModel.getPlayer().keyReleased(e, view);
        }
    }

    public myKeyboard getMyKeyboard(){
        return new myKeyboard();
    }

}
