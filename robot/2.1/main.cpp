#include <locale>
#include <fstream>

#include "Surface.hpp"
#include "SpaceImpl.hpp"
#include "Cylinder.hpp"
#include "Planar.hpp"
#include "Tor.hpp"
#include "Robot.hpp"
#include "SurfaceFactory.hpp"
#include "anyoption.h"


int main(int argc, char** argv)
{
	AnyOption *Options = new AnyOption();////������ ��������� ������ 
	setlocale(0, "Russian");
	Options->setFlag("help", 'h');
	Options->setOption("space", 's');
	Options->setOption("out", 'o');
	Options->setOption("limit", 'l');
	Options->setOption("topology", 't');
	Options->processCommandArgs(argc, argv);
	string topology;
	string fileInput;
	string fileOut;
	uint limit = 1000;

	if (Options->getFlag("help") || Options->getFlag('h')) {
		std::cout << "(-h --help) - ������ ��������� �� ������� ���������� ��������� � ���������� ������." << std::endl;
		std::cout << "(-s --space) - ��� ���������� ����� � ��������� ������������. (space.txt �� ���������)" << std::endl;
		std::cout << "(-o --out) - �������� ��������� ���� � ���������. (route.txt �� ���������)" << std::endl;
		std::cout << "(-l --limit) - ����� ������������ ����� ���������. ���� ������� ��������� ������ ����� �� �������������. (1000 �� ���������)" << std::endl;
		std::cout << "(-t --topology) - ������������, ������� ���������� ��������� ������������. (planar �� ���������)" << std::endl;
		return _HELP_CALLED_;
	}

	if ((Options->getValue("topology") != NULL) || (Options->getValue('t') != NULL)) {
		if (strcmp(Options->getValue('t'), "planar") == 0)
			topology = "planar";
		else if (strcmp(Options->getValue('t'), "cylinder") == 0)
			topology = "cylinder";
		else if (strcmp(Options->getValue('t'), "tor") == 0)
			topology = "tor";
		else {
			topology = "planar";
		}
	}

	if ((Options->getValue("limit") != NULL) || (Options->getValue('l') != NULL)) {
		limit = atoi(Options->getValue('l'));
		if (limit == 0)
		{
			cout << "You entered incorrect limit number. It'll be used default limit (1000)." << endl;
			limit = 1000;
		}
	}

	if ((Options->getValue("space") != NULL) || (Options->getValue('s') != NULL))
		fileInput = Options->getValue('s');
	else
		fileInput = "space.txt";

	if ((Options->getValue("out") != NULL) || (Options->getValue('o') != NULL))
		fileOut = Options->getValue('o');
	else
		fileOut = "route.txt";

	std::ifstream from(fileInput);
	if (!from.good())
	{
		std::cout << "Error open file: " << "\"" << fileInput << "\"" << std::endl;
		return _INPUT_FILE_OPEN_ERROR_;
	}
	SpaceImpl* space = SurfaceFactory::getInstance().getSpaceImpl(topology);
	from >> *space;
	std::ofstream to(fileOut);

	if (!to.good()) 
	{
		std::cout << "Error open file: " << "\"" << fileOut << "\"" << std::endl;
		return _OUTPUT_FILE_OPEN_ERROR_;
	}

	Robot robot(space);
	try 
	{
		robot.research();
	}
	catch (...)
	{
		std::cout << "Path not found" << std::endl;
		to << "Path not found" << std::endl;
		delete space;
		return _BADMOVE_EXCEPTION_;
	}
	to << *space;
	delete space;
	delete Options;
	return 0;
}