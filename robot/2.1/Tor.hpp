#ifndef _TOR_
#define _TOR_

#include "SpaceImpl.hpp"
class Tor :
	public SpaceImpl
{
protected:
	virtual uint distanceToFinish(const Point fromPoint);

public:
	virtual std::vector <std::tuple<Point, uint>> lookup();
};

#endif