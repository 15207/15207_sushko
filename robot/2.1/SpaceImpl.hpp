#ifndef _SPACEIMPL_
#define _SPACEIMPL_

#include <cstdlib>

#include "Surface.hpp"
//#include <fstream>

class SpaceImpl :
	public Surface
{
protected:
	Point start;
	Point finish;
	Point current;
	std::vector<std::string> field;
	virtual uint distanceToFinish(const Point fromPoint) = 0;
public:
	virtual uint move(const Point point);
	//void setInCurrentStar(const char data);
	Point getCurrent() const;
	Point getFinish() const;


	friend std::istream& operator >> (std::istream&, SpaceImpl&);
	friend std::ostream& operator << (std::ostream&, const SpaceImpl&);
};

std::istream& operator >> (std::istream&, SpaceImpl&);
std::ostream& operator << (std::ostream&, const SpaceImpl&);

#endif