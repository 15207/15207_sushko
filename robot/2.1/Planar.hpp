#ifndef _PLANAR_
#define _PLANAR_

#include "SpaceImpl.hpp"
class Planar :
	public SpaceImpl
{
protected:
	virtual uint distanceToFinish(const Point fromPoint);

public:
	virtual std::vector <std::tuple<Point, uint>> lookup();
};

#endif