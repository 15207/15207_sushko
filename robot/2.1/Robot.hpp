#ifndef _ROBOT_
#define _ROBOT_

#include "SpaceImpl.hpp"
class Robot 
{
	SpaceImpl *space;
	uint limit;
	Point getClosestPath(const std::vector<std::tuple<Point, uint>> tuple);

public:
	Robot(SpaceImpl *sp, uint lim = 1000) : space(sp), limit(lim) {};
	void research();
};

#endif