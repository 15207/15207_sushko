#ifndef _CYLINDER_
#define _CYLINDER_

#include "SpaceImpl.hpp"
class Cylinder :
	public SpaceImpl
{
protected:
	virtual uint distanceToFinish(const Point fromPoint);
public:
	virtual std::vector <std::tuple<Point, uint>> lookup();
};

#endif