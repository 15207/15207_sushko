#include "Surface.hpp"

Surface<std::string, uint>::Surface(std::istream &from, std::string dictFile) 
{
	from >> *this;

	this->dict = dictFile;
	this->isRead = false;
	this->cache.reserve(distance(start, finish) - 1);

	this->path.push_back(start);
}

Surface<std::string, uint>::~Surface() 
{
	cache.clear();
	path.clear();

	cache.~vector();
	path.~vector();
}

size_t Surface<std::string, uint>::distance(std::string &from, std::string &to)
{
	const uint len1 = from.size(),
		len2 = to.size();

	std::vector<uint> col(len2 + 1),
		prevCol(len2 + 1);

	for (uint i = 0; i < prevCol.size(); i++)
	{
		prevCol[i] = i;
	}

	for (uint i = 0; i < len1; i++)
	{
		col[0] = i + 1;

		for (uint j = 0; j < len2; j++)
		{
			col[j + 1] = fmin(fmin(prevCol[1 + j] + 1, col[j] + 1), prevCol[j] + (from[i] == to[j] ? 0 : 1));
		}
		col.swap(prevCol);
	}

	return prevCol[len2];
}

std::vector<std::tuple<std::string, uint>> Surface<std::string, uint>::lookup() throw (FileError) 
{
	std::vector<std::tuple<std::string, uint>> result;

	size_t maxDest = distance(start, finish);
	size_t distanc = SIZE_MAX;
	std::ifstream dictFile;
	std::string word;
	uint currInd;

	if (!isRead) 
	{
		dictFile.open(dict);

		if (!dictFile.good())
		{
			throw FileError();
		}
		while (!dictFile.eof()) 
		{
			dictFile >> word;

			if (((distanc = distance(start, word)) > 0) && (distanc <= cache.size()) && (distance(word, finish) < distance(start, finish)))
			{
				cache[distanc - 1].push_back(word);
			}

		}

		dictFile.close();
		isRead = true;
	}

	distanc = distance(curr, finish);
	currInd = distance(start, finish) - distanc;

	for (uint i = 0; i < cache[currInd].size(); ++i) 
	{
		result.push_back(make_tuple(cache[currInd][i], currInd + 1));
	}

	return result;
}

std::tuple<std::string, uint> Surface<std::string, uint>::getClosest(std::vector<std::tuple<std::string, uint>> &lookupResults) 
{
	uint dist, minDist = SIZE_MAX;
	std::string needStr;

	if (!lookupResults.size()) 
	{
		return (std::make_tuple(start, 0));
	}
	else {
		for (uint i = 0; i < lookupResults.size(); ++i) 
		{
			dist = distance(std::get<0>(lookupResults[i]), finish);

			if (dist <= minDist) 
			{
				minDist = dist;
				needStr = std::get<0>(lookupResults[i]);

				if (std::get<0>(lookupResults[i]) == finish)
				{
					break;
				}
			}
		}
	}

	return (make_tuple(needStr, minDist));
}

uint Surface<std::string, uint>::move(std::string &word) throw (BadMove) {
	if (word == start || word == curr)
	{
		throw BadMove();
	}

	curr = word;
	path.push_back(word);

	return distance(word, finish);
}

void Robot<std::string, uint>::research() 
{
	std::tuple<std::string, uint> next;
	std::string wNext;

	do 
	{
		next = space->getClosest(space->lookup());
		wNext = std::get<0>(next);
	} while (space->move(wNext));
}


void Robot<std::string, uint>::showPath(std::ostream &to) 
{
	to << *(this->space);
}

std::ostream& operator<<(std::ostream& to, Surface<std::string, uint> &space) 
{
	for (uint i = 0; i < space.path.size(); ++i)
	{
		to << " " << space.path[i] << ((i == space.path.size() - 1) ? "" : " ->");
	}

	return to;
}

std::istream& operator >> (std::istream &from, Surface<std::string, uint> &space) 
{
	from >> space.start;
	from >> space.finish;
	space.curr = space.start;

	space.cache = std::vector<std::vector<std::string>>(space.distance(space.start, space.finish));

	return from;
}