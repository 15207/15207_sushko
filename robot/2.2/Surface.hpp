#ifndef _SURFACE_
#define _SURFACE_

#include <iostream>
#include <vector>
#include <tuple>
#include <fstream>
#include <string>
#include <limits>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include <cstring>

const int _OUTPUT_FILE_OPEN_ERROR_ = -100;
const int _INPUT_FILE_OPEN_ERROR_ = -101;
const int _UNKNOWN_TOPOLOGY_TYPE_ = -102;
const int _BADMOVE_EXCEPTION_ = -103;
const int _GT_ALLOWABLE_LIMIT_ = -104;
const int _DICTIONARY_FILE_OPEN_ERROR = -105;

const int _HELP_CALLED_ = 101;

typedef unsigned int uint;
typedef unsigned char uchar;

struct Point {
	int x;
	int y;
};

struct Config {
	bool help = false;
	std::string space = "space.txt";
	std::string out = "route.txt";
	std::string dict = "dictionary.txt";
	int limit = 1000;
	std::string topology = "planar";
};

class BadMove : public std::exception {};
class FileError : public std::exception {};

Config readParameters(int argc, char** argv);
void callHelp();

template <typename P, typename M>
class Surface
{
public:
	Surface(std::istream &from);

	M distance(P &from, P &to);
	std::tuple<P, M> getClosest(std::vector<std::tuple<P, M>> &lookupResults);
	M research();

	M move(P &point) throw (BadMove);
	std::vector<std::tuple<P, M>> lookup();
};

template<>
class Surface <Point, uint>
{
public:
	Point start, finish, curr;
	std::vector<std::vector<uchar>> matrix;
	Surface(std::istream &from);
	~Surface();

	uint distance(Point &from, Point &to);
	std::tuple<Point, uint> getClosest(std::vector<std::tuple<Point, uint>> &lookupResults);

	uint move(Point &point) throw (BadMove);
	std::vector<std::tuple<Point, uint>> lookup();
};

template<>
class Surface <std::string, uint> {
public:
	std::string start, finish, curr, dict;
	std::vector<std::vector<std::string>> cache;
	std::vector<std::string> path;
	bool isRead;

	Surface(std::istream &from, std::string dictFile);
	~Surface();

	uint distance(std::string &from, std::string &to);
	std::tuple<std::string, uint> getClosest(std::vector<std::tuple<std::string, uint>> &lookupResults);

	uint move(std::string &word) throw (BadMove);
	std::vector<std::tuple<std::string, uint>> lookup() throw (FileError);
};

template<class P, class M>
class Robot {
	Surface<P, M> *space;

public:
	Robot(Surface<P, M> *space) : space(space) {};

	void research();
	void showPath(std::ostream &to);

};

std::ostream& operator<<(std::ostream &to, Surface<Point, uint> &space);
std::istream& operator >> (std::istream &from, Surface<Point, uint> &space);

std::ostream& operator<<(std::ostream &to, Surface<std::string, uint> &space);
std::istream& operator >> (std::istream &from, Surface<std::string, uint> &space);

#endif