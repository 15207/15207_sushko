/**
 * Created by Archangel on 15.02.2017.
 */
import java.lang.StringBuilder;
import java.io.*;
import java.util.*;

public class StrStat {
    private MapAndNumb cl;
    public static void main(String[] args) {
            StrStat man = new StrStat();
            try
            {
                man.cl = man.saveMap(args[0]);
                man.print(man.cl, args[0]);
            }
            catch (IOException e)
            {
                System.err.println("Error while reading file: " + e.getLocalizedMessage());
            }

    }

    private MapAndNumb saveMap(String args) throws IOException{
        Reader reader = null;
        try {
            reader = new InputStreamReader(new FileInputStream(args), "UTF-16");
        } catch (IOException e)
        {
            if (null != reader) {
                try {
                    reader.close();
                    throw e;
                } catch (IOException i) {
                    e.printStackTrace(System.err);
                    throw i;
                }
            }
        }
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        StringBuilder buffer = new StringBuilder();
        int temp = reader.read();
        char temp_too;
        int kolvo = 0;
        while (temp != -1){
            temp_too = (char) temp;
            if (Character.isLetterOrDigit(temp_too)){
                buffer.append(temp_too);
            }
            else {
                 if(buffer.length() != 0){
                     if (map.containsKey(buffer.toString())){
                         map.put(buffer.toString(), map.get(buffer.toString()) + 1);
                     } else {
                         map.put(buffer.toString(), 1);
                     }
                     kolvo++;
                 }
                buffer.delete(0, buffer.length());

            }
            temp = reader.read();
        }
        if (buffer.length() != 0){
                if (map.containsKey(buffer.toString())){
                    map.put(buffer.toString(), map.get(buffer.toString()) + 1);
                } else {
                    map.put(buffer.toString(), 1);
                }
            ++kolvo;
            buffer.delete(0, buffer.length());

        }
        MapAndNumb cl = new MapAndNumb();
        cl.map = map;
        cl.kolvo = kolvo;
        reader.close();
        return cl;
    }

    private int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
        return a.getValue() - b.getValue();
    }

    private void print(MapAndNumb cl, String filename) throws IOException{
        List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(cl.map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {//также предупреждение
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return b.getValue() - a.getValue();
            }
        });
        int summ = 0;
        FileWriter writer = new FileWriter(filename + ".csv");
        for(int i=0; i < list.size(); ++i){
            writer.write(list.get(i).getKey() + "\t" + list.get(i).getValue() + "\t" +
                    ((double)list.get(i).getValue() * 100.0 /(double)cl.kolvo) + "\n");
        }
        writer.close();
    }
}

class MapAndNumb {
    public Map<String, Integer> map;
    public int kolvo;
}
